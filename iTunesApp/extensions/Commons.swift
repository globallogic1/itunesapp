//
//  Commons.swift
//  ItunesPagerApp
//
//  Created by slacker on 1/29/20.
//  Copyright © 2020 Juan Pablo. All rights reserved.
//

import UIKit

extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}

extension UIViewController {
    func showLoading() {
        
    }
    func hideLoading() {
        
    }
}

