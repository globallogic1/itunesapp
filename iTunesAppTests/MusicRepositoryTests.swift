//
//  MusicRepositoryTests.swift
//  ItunesPagerAppTests
//
//  Created by slacker on 1/31/20.
//  Copyright © 2020 Juan Pablo. All rights reserved.
//

import XCTest
import CoreData
@testable import iTunesApp

class MusicRepositoryTests: XCTestCase {

    var musicApi: MusicApiSpy!
    var dataRepository: TrackDataRepositorySpy!
    var sut: ItunesRepository!

    var emptyOnlineSearch = TextSearch(text: "", online: true)
    var emptyOfflineSearch = TextSearch(text: "", online: false)

    var emptyResult = [Track]()
    var emptyTrack = Track(wrapperType: "",
                           collectionId: 0,
                           artistName: "",
                           collectionName: "",
                           trackName: "",
                           previewUrl: "",
                           artworkUrl100: "")
    
    var trackWithName = Track(wrapperType: "",
                              collectionId: 0,
                              artistName: "",
                              collectionName: "",
                              trackName: "A Song",
                              previewUrl: "",
                              artworkUrl100: "")
    
    override func setUp() {
        musicApi = MusicApiSpy()
        dataRepository = TrackDataRepositorySpy()
        sut = ItunesRepository(api: musicApi, store: dataRepository)
    }

    override func tearDown() {
        musicApi = nil
        dataRepository = nil
        sut = nil
    }
    
    func testThatEmptyResultsDontGetStored() {
        var tracks: [[Track]]?
        
        sut.getTracks(by: emptyOnlineSearch) { result in
            switch result {
                case .success(let pages): tracks = pages.pages
                case _: XCTFail("empty results should be ok")
            }
        }

        XCTAssertNotNil(tracks)
        XCTAssertNil(dataRepository.tracks, "local store should no save empty results")
    }

    func testThatDataStoreWasCalledWithNonEmptyResults() {
        musicApi.data = [Track](arrayLiteral: emptyTrack)

        sut.getTracks(by: emptyOnlineSearch) { _ in }

        XCTAssert(dataRepository.insertWasCalled)
    }

    func testThatForNamedTracksThereArePagesEveryTwentyTracks() {
        let oneHundredTracks = [Track](repeating: trackWithName, count: 100)
        musicApi.data = oneHundredTracks

        var container: Page<[Track]>!
        sut.getTracks(by: emptyOnlineSearch) { result in
            switch result {
                case .success(let pages): container = pages
                case _: XCTFail("there should be result")
            }
        }

        XCTAssertEqual(container.pages.count, 5)
    }
    
    func testThatTracksWithoutNameDontCount() {
        let oneHundredTracks = [Track](repeating: emptyTrack, count: 100)
        musicApi.data = oneHundredTracks

        var container: Page<[Track]>!
        sut.getTracks(by: emptyOnlineSearch) { result in
            switch result {
                case .success(let pages): container = pages
                case _: XCTFail("there should be result")
            }
        }

        XCTAssertEqual(container.pages.count, 0)
    }

    func testThatOfflineSearchWillNotGoingToCallTheRemoteRepository() {
        musicApi.data = [Track](arrayLiteral: emptyTrack)

        sut.getTracks(by: emptyOfflineSearch) { _ in }

        XCTAssertFalse(musicApi.getTracksWasCalled, "cant be called on offline searches")
    }
    
    func testThatOnlyTheLocalRepositoryIsCalled() {
        musicApi.data = [Track](arrayLiteral: emptyTrack)

        sut.getTracks(by: emptyOfflineSearch) { _ in }

        XCTAssertFalse(dataRepository.insertWasCalled, "can not insert offline findings")
        XCTAssertTrue(dataRepository.findWasCalled, "find has to be called if it is offline")
    }
}
