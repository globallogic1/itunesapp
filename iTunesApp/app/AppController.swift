//
//  AppController.swift
//  iTunesApp
//
//  Created by slacker on 1/31/20.
//  Copyright © 2020 jp. All rights reserved.
//

import UIKit

struct Environment {
    let urlString: String
}

class AppCoordinator {
    private let environment: Environment
    private let rootViewController = UINavigationController()

    init(environment: Environment) {
        self.environment = environment
    }

    func start(window: UIWindow) {
        rootViewController.pushViewController(initialViewController(), animated: true)

        window.rootViewController = rootViewController
        window.backgroundColor = .white
        window.makeKeyAndVisible()
    }

    private func makeRepository() -> MusicRepository {
        let noCacheConfig = URLSessionConfiguration.default
        noCacheConfig.requestCachePolicy = .reloadIgnoringLocalCacheData
        noCacheConfig.urlCache = nil
        
        let url = URL(string: environment.urlString)!
        let client = RestClient(manager: URLSession(configuration: noCacheConfig), url: url)
        let api = ItunesApi(client: client)

        let dataStack = CoreDataStack(modelName: "iTunesModel")
        let dataRepository = TrackDataRepository(coreDataStack: dataStack)

        return ItunesRepository(api: api, store: dataRepository)
    }

    private func initialViewController() -> UIViewController {
        let view = TracksView()
        let presenter = TracksPresenter(repository: makeRepository())

        view.presenter = presenter
        presenter.view = view

        presenter.finishWithAlbumForTrack = { [weak self] tracks in
            DispatchQueue.main.async {
                self?.showAlbumTracks(for: tracks)
            }
        }
        
        return view
    }

    private func showAlbumTracks(for tracks: [Track]) {
        let view = AlbumView()
        view.tracks = tracks

        rootViewController.pushViewController(view, animated: true)
    }
}
