//
//  TrackDataRepositorySpy.swift
//  iTunesAppTests
//
//  Created by slacker on 1/31/20.
//  Copyright © 2020 jp. All rights reserved.
//

import CoreData
@testable import iTunesApp

class TrackDataRepositorySpy: DataRepository {
    var tracks: [Track]?
    var insertWasCalled = false
    var findWasCalled = false

    func insert(tracks: [Track]) {
        self.tracks = tracks

        insertWasCalled = true
    }
    
    func find(with request: NSFetchRequest<TrackEntity>) -> [Track]? {
        findWasCalled = true
        
        return tracks
    }
}
