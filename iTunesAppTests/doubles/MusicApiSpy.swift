//
//  MusicApiSpy.swift
//  iTunesAppTests
//
//  Created by slacker on 1/31/20.
//  Copyright © 2020 jp. All rights reserved.
//

@testable import iTunesApp

class MusicApiSpy: MusicApi {
    var data: [Track]?

    var getTracksWasCalled = false
    var findAlbumWasCalled = false
    
    func getTracks(by terms: SearchTerms, result: @escaping ((Result<[Track], Error>) -> ())) {
        getTracksWasCalled = true

        guard let data = data else {
            result(.success([]))
            return
        }
        
        result(.success(data))
    }
    
    func findAlbum(by track: Track, result: @escaping ((Result<[Track], Error>) -> ())) {
        findAlbumWasCalled = true
    }
}
