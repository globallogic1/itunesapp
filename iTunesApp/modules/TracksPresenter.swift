//
//  TracksPresenter.swift
//  ItunesPagerApp
//
//  Created by slacker on 1/29/20.
//  Copyright © 2020 Juan Pablo. All rights reserved.
//

import Foundation

struct TextSearch {
    let text: String
    let online: Bool
}

struct TrackSearch {
    let track: Track
    let online: Bool
}

protocol TracksDisplayable: class {
    var presenter: TracksPresentable? { get set }

    func display(pages: Page<[Track]>)
    func display(error message: String)

    func suggestOfflineTrackSearch(text: String)
    func suggestOfflineAlbumSearch(track: Track)
}

protocol TracksPresentable: class {
    var view: TracksDisplayable? { get set }

    func search(by: TextSearch)
    func findAlbum(by: TrackSearch)
}

class TracksPresenter {
    private let repository: MusicRepository

    weak var view: TracksDisplayable?

    var finishWithAlbumForTrack: (([Track]) -> ())?

    private var pages: Page<[Track]>! {
        didSet {
            view?.display(pages: pages)
        }
    }

    init(repository: MusicRepository) {
        self.repository = repository
    }
    
    private func filterTracks(error: NSError, text: String) {
        if error.code == NSURLErrorNotConnectedToInternet {
            view?.suggestOfflineTrackSearch(text: text)
            return
        }

        view?.display(error: "There was a problem with your track search.\n\nTry again.")
    }

    private func filterAlbums(error: NSError, track: Track) {
        if error.code == NSURLErrorNotConnectedToInternet {
            view?.suggestOfflineAlbumSearch(track: track)
            return
        }

        view?.display(error: "There was a problem with your album search.\n\nTry again.")
    }
}
extension TracksPresenter: TracksPresentable {
    func search(by textSearch: TextSearch) {
        repository.getTracks(by: textSearch) { [weak self] result in
            switch result {
                case .success(let pages): self?.pages = pages
                case .failure(let error as NSError): self?.filterTracks(error: error, text: textSearch.text)
            }
        }
    }

    func findAlbum(by trackSearch: TrackSearch) {
        repository.findAlbum(by: trackSearch) { [weak self] result in
            switch result {
                case .success(let album): self?.finishWithAlbumForTrack?(album)
                case .failure(let error as NSError): self?.filterAlbums(error: error, track: trackSearch.track)
            }
        }
    }
}
