//
//  MusicApi.swift
//  ItunesPagerApp
//
//  Created by Juan Pablo on 29-01-20.
//  Copyright © 2020 Juan Pablo. All rights reserved.
//

import Foundation

protocol MusicApi {
    func getTracks(by terms: SearchTerms, result: @escaping ((Result<[Track], Error>) -> ()))
    func findAlbum(by track: Track, result: @escaping ((Result<[Track], Error>) -> ()))
}

class ItunesApi {
    private let client: ApiClient

    init(client: ApiClient) {
        self.client = client
    }
}
extension ItunesApi: MusicApi {
    func getTracks(by terms: SearchTerms, result: @escaping ((Result<[Track], Error>) -> ())) {
        client.request(API.Media.searchTracks(by: terms)) { response in
            switch response {
                case .success(let nest): result(.success(nest.results))
                case .failure(let error): result(.failure(error))
            }
        }
    }

    func findAlbum(by track: Track, result: @escaping ((Result<[Track], Error>) -> ())) {
        let payload = SearchAlbum(id: track.collectionId)
        
        client.request(API.Media.findAlbum(by: payload)) { response in
            switch response {
                case .success(let nest): result(.success(nest.results))
                case .failure(let error): result(.failure(error))
            }
        }
    }
}
