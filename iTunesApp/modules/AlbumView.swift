//
//  AlbumView.swift
//  ItunesPagerApp
//
//  Created by slacker on 1/30/20.
//  Copyright © 2020 Juan Pablo. All rights reserved.
//

import UIKit
import AVFoundation

class AlbumView: UIViewController {

    @IBOutlet weak var albumImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!

    private let player = AVQueuePlayer()
    
    var tracks: [Track] = [] {
        didSet {
            details = tracks.first

            songs.append(contentsOf: Array(tracks.dropFirst()))
        }
    }

    private var details: Track?
    private var songs: [Track] = []

    private var isPlaying: Bool = false
    
    private var lastItemPlayed: (indexPath: IndexPath, item: AVPlayerItem)? {
        didSet {
            player.removeAllItems()
            player.insert(lastItemPlayed!.item, after: nil)
            player.play()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Band: \(details?.artistName ?? "")"

        setupViews()
    }

    private func setupViews() {
        albumImageView.image = details
            .flatMap { URL(string: $0.artworkUrl100) }
            .flatMap { try? Data(contentsOf: $0) }
            .flatMap { UIImage(data: $0) }
    
        
        NotificationCenter
            .default
            .addObserver(self, selector: #selector(changeLastItemState), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }

    @objc func changeLastItemState(_ sender: Notification) {
        guard let indexPath = lastItemPlayed?.indexPath else {
            return
        }

        isPlaying = false
        tableView.cellForRow(at: indexPath)?.imageView?.image = UIImage(named: "icon_pause")
    }
}
extension AlbumView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.text = "Album: \(details?.collectionName ?? "")"
        label.textAlignment = .center

        return label
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let urlString = songs[indexPath.row].previewUrl, let url = URL(string: urlString) else {
            return
        }

        if let lastItem = lastItemPlayed {
            if lastItem.indexPath == indexPath && isPlaying {
                tableView.cellForRow(at: indexPath)?.imageView?.image = UIImage(named: "icon_pause")
                player.removeAllItems()
                isPlaying = false
                return
            }

            tableView.cellForRow(at: lastItem.indexPath)?.imageView?.image = UIImage(named: "icon_pause")
        }

        tableView.cellForRow(at: indexPath)?.imageView?.image = UIImage(named: "icon_play")
        lastItemPlayed = (indexPath, AVPlayerItem(url: url))
        isPlaying = true
    }
}

extension AlbumView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()

        cell.textLabel?.text = songs[indexPath.row].trackName
        cell.imageView?.image = UIImage(named: "icon_pause")
        return cell
    }
}
