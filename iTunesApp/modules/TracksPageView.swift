//
//  TracksPageView.swift
//  ItunesPagerApp
//
//  Created by slacker on 1/30/20.
//  Copyright © 2020 Juan Pablo. All rights reserved.
//

import UIKit

class TracksPageView: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var tracks: [Track] = []
    
    var finishWithSelectedTrack: ((Track) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
extension TracksPageView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()

        cell.textLabel?.text = tracks[indexPath.row].trackName
        
        return cell
    }
}
extension TracksPageView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        finishWithSelectedTrack?(tracks[indexPath.row])
    }
}
