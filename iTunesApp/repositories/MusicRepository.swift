//
//  MusicRepository.swift
//  ItunesPagerApp
//
//  Created by slacker on 1/29/20.
//  Copyright © 2020 Juan Pablo. All rights reserved.
//

import Foundation

struct Search {
    let text: String
}

struct Album {
    let logo: URL
    let tracks: [Track]
}

struct Page<T: Decodable> {
    let pages: [T]
}

protocol MusicRepository {
    func getTracks(by search: TextSearch, result: @escaping ((Result<Page<[Track]>, Error>) -> ()))
    func findAlbum(by search: TrackSearch, result: @escaping ((Result<[Track], Error>) -> ()))
}

class ItunesRepository {
    private let api: MusicApi
    private let store: DataRepository

    init(api: MusicApi, store: DataRepository) {
        self.api = api
        self.store = store
    }
}
extension ItunesRepository: MusicRepository {
    func getTracks(by search: TextSearch, result: @escaping ((Result<Page<[Track]>, Error>) -> ())) {
        if !search.online {
            guard let tracks = store.find(with: TrackEntity.fetchTracks(by: search.text)) else {
                result(.success(Page<[Track]>(pages: [])))
                return
            }

            let pages = Page(pages: tracks
                .filter { !($0.trackName ?? "").isEmpty }
                .chunked(into: 20))
            
            result(.success(pages))
            return
        }
        
        let terms = SearchTerms(terms: search.text)

        api.getTracks(by: terms) { [weak self] response in
            switch response {
                case .success(let tracks):
                    if !tracks.isEmpty {
                        self?.store.insert(tracks: tracks)
                    }

                    let pages = Page(pages: tracks
                        .filter { !($0.trackName ?? "").isEmpty }
                        .chunked(into: 20))

                    result(.success(pages))
                case .failure(let error):
                    result(.failure(error))
            }
        }
    }
    
    func findAlbum(by search: TrackSearch, result: @escaping ((Result<[Track], Error>) -> ())) {
        if !search.online {
            guard let tracks = store.find(with: TrackEntity.fetchTracks(by: search.track.collectionId)) else {
                result(.success([]))
                return
            }

            result(.success(tracks))
            return
        }

        api.findAlbum(by: search.track) { [weak self] response in
            switch response {
                case .success(let tracks):
                    if !tracks.isEmpty {
                        self?.store.insert(tracks: tracks)
                    }

                    result(.success(tracks))
                case .failure(let error): result(.failure(error))
            }
        }
    }
}

