//
//  AppDelegate.swift
//  iTunesApp
//
//  Created by slacker on 1/31/20.
//  Copyright © 2020 jp. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    var app: AppCoordinator = {
        return AppCoordinator(environment: Environment(urlString: "https://itunes.apple.com"))
    }()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)

        app.start(window: window!)
        
        return true
    }
}

