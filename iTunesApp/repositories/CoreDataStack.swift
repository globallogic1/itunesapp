//
//  CoreDataStack.swift
//  ItunesPagerApp
//
//  Created by slacker on 1/30/20.
//  Copyright © 2020 Juan Pablo. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStack {
    private let modelName: String

    init(modelName: String) {
        self.modelName = modelName
    }

    private lazy var storeContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: self.modelName)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                print("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    lazy var managedContext: NSManagedObjectContext = {
        return self.storeContainer.viewContext
    }()

    func saveContext() {
        guard managedContext.hasChanges else { return }

        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Unresolved error \(error), \(error.userInfo)")
        }
    }
}

protocol DataRepository {
    func insert(tracks: [Track])
    func find(with request: NSFetchRequest<TrackEntity>) -> [Track]?
}

class TrackDataRepository {
    let coreDataStack: CoreDataStack

    lazy var managedContext: NSManagedObjectContext = {
        return coreDataStack.managedContext
    }()

    init(coreDataStack: CoreDataStack) {
        self.coreDataStack = coreDataStack
    }

    private func entity(for track: Track) {
        let entity = TrackEntity(context: managedContext)
        entity.wrapperType = track.wrapperType
        entity.artistName = track.artistName
        entity.collectionId = track.collectionId
        entity.artworkUrl100 = track.artworkUrl100
        entity.collectionName = track.collectionName
        entity.previewUrl = track.previewUrl
        entity.trackName = track.trackName

        managedContext.insert(entity)
    }

    private func track(for entity: TrackEntity) -> Track {
        return Track(wrapperType: entity.wrapperType,
                     collectionId: entity.collectionId,
                     artistName: entity.artistName,
                     collectionName: entity.collectionName,
                     trackName: entity.trackName,
                     previewUrl: entity.previewUrl,
                     artworkUrl100: entity.artworkUrl100)
    }

    private func find(for request: NSFetchRequest<TrackEntity>) -> [TrackEntity]? {
        do {
            return try managedContext.fetch(request)
        } catch let error as NSError {
            print("Colud not retrieve cards: \(error), \(error.userInfo)")
            return nil
        }
    }
}
extension TrackDataRepository: DataRepository {
    func insert(tracks: [Track]) {
        tracks.forEach { entity(for: $0) }
        
        coreDataStack.saveContext()
    }
    
    func find(with request: NSFetchRequest<TrackEntity>) -> [Track]? {
        return find(for: request)?.map { track(for: $0) }
    }
}
