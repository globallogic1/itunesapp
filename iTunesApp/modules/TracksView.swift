//
//  ViewController.swift
//  ItunesPagerApp
//
//  Created by Juan Pablo on 29-01-20.
//  Copyright © 2020 Juan Pablo. All rights reserved.
//

import UIKit

class TracksView: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    private let searchController = UISearchController(searchResultsController: nil)
    private var pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)

    private var pages = [UIViewController]() {
        didSet {
            setupPages()
        }
    }

    private var pendingIndex: Int?

    var presenter: TracksPresentable?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Songs"
        
        setupViews()
    }

    private func setupViews() {
        searchController.searchBar.placeholder = "Search tracks"
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.enablesReturnKeyAutomatically = true
        searchController.searchBar.delegate = self

        navigationItem.searchController = searchController
        definesPresentationContext = true
    }

    private func setupPages(pages: Page<[Track]>) {
        let pages = pages.pages.map { [weak self] tracks -> UIViewController in
            let view = TracksPageView()
            view.tracks = tracks

            view.finishWithSelectedTrack = { track in
                self?.presenter?.findAlbum(by: TrackSearch(track: track, online: true))
            }

            return view
        }

        self.pages = pages
    }
    
    private func setupPages() {
        guard let first = pages.first else {
            return
        }

        pageControl.currentPageIndicatorTintColor = .blue
        pageControl.pageIndicatorTintColor = UIColor.blue.withAlphaComponent(0.3)

        pageControl.numberOfPages = pages.count
        pageControl.currentPage = 0
        
        pageControl.isUserInteractionEnabled = false

        pageViewController.setViewControllers([first], direction: .forward, animated: true, completion: nil)
        
        pageViewController.delegate = self
        pageViewController.dataSource = self
        
        addChild(pageViewController)
        containerView.addSubview(pageViewController.view)

        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            pageViewController.view.topAnchor.constraint(equalTo: containerView.topAnchor),
            pageViewController.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            pageViewController.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            pageViewController.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor)
        ])
        
        pageViewController.didMove(toParent: parent)
    }

    private func updateCurrentView(index: Int, direction: UIPageViewController.NavigationDirection) {
        pageControl.currentPage = index

        pageViewController.setViewControllers([pages[index]], direction: direction, animated: true, completion: nil)
    }

}

extension TracksView: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let search = searchBar.text, !search.isEmpty else {
            return
        }

        presenter?.search(by: TextSearch(text: search, online: true))
    }
}

extension TracksView: TracksDisplayable {
    func display(pages: Page<[Track]>) {
        DispatchQueue.main.async {
            self.hideLoading()
            self.setupPages(pages: pages)
        }
    }
    
    func display(error message: String) {
        DispatchQueue.main.async {
            self.hideLoading()
        }
    }

    private func suggestion(with action: UIAlertAction) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: "No Internet Connection", message: "¿Do you wish a local search?", preferredStyle: .alert)

            alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alertController.addAction(action)

            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func suggestOfflineTrackSearch(text: String) {
        suggestion(with: UIAlertAction(title: "Continue", style: .default){ [weak self] action in
            self?.presenter?.search(by: TextSearch(text: text, online: false))
        })
    }

    func suggestOfflineAlbumSearch(track: Track) {
        suggestion(with: UIAlertAction(title: "Accept", style: .default){ [weak self] action in
            self?.presenter?.findAlbum(by: TrackSearch(track: track, online: false))
        })
    }
}

extension TracksView: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pages.firstIndex(of: viewController) else { return nil }
        if currentIndex == 0 { return nil }

        return pages[abs((currentIndex - 1) % pages.count)]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pages.firstIndex(of: viewController) else { return nil }
        if currentIndex == pages.count - 1 { return nil }

        return pages[abs((currentIndex + 1) % pages.count)]
    }

    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        pageControl.currentPage
    }
}

extension TracksView: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        pendingIndex = pages.firstIndex(of: pendingViewControllers.first!)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {

        if !completed { return }
        guard let index = pendingIndex else { return }

        pageControl.currentPage = index
    }
}
